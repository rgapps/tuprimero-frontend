export const userNavigation = {
  "/admin/*": [
    {text: "Resources", to: "/admin"},
    {text: "Orders", to: "/admin/orders"},
    {text: "Configuration", to: "/admin/configuration"},
  ],
  "*": [
    {text: "Home", to: "/"},
    {text: "Products", to: "/products"},
    {text: "Contact", to: "/contact"},
    {text: "About us", to: "/about"},
  ],
};

export const socialLinks = {
  "/admin/*": [],
  "*": [
    {
      icon: "mdi-linkedin",
      link: "https://www.linkedin.com/company/89863251/",
      color: "indigo darken-4",
    },
    {
      icon: "mdi-instagram",
      link: "https://www.instagram.com/tuprimero.uae/",
      color: "purple accent-4",
    },
    {
      icon: "mdi-facebook",
      link: "https://www.facebook.com/profile.php?id=100088200368630",
      color: "blue-darken-2",
    },
  ],
};
