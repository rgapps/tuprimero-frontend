const clickOutside = (el, binding, vnode) => {
  document.addEventListener("click", (event) => {
    if (event.target !== el) {
      vnode.context[binding.expression](event);
    }
  });
};

export default clickOutside;
