import {reactive, toRefs} from "vue";

const useHttpDelete = (uri) => {
  const state = reactive({
    response: null,
    error: null,
    loading: false,
    status: 0,
  });

  const request = (id) => {
    state.loading = true;
    fetch(`${uri}/${id}`, {
      method: "DELETE",
    })
      .then((res) => {
        state.status = res.status;
        return res.json();
      })
      .then((resBody) => {
        state.response = resBody;
      })
      .catch((err) => {
        state.error = err;
        if (err.response) state.status = err.response.status;
      })
      .finally(() => {
        state.loading = false;
      });
  };

  return {...toRefs(state), request};
};

export default useHttpDelete;
