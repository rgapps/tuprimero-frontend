import {reactive, toRefs} from "vue";

const useHttpPut = (uri) => {
  const state = reactive({
    response: null,
    error: null,
    loading: false,
    status: 0,
  });

  const request = (id, body) => {
    state.loading = true;
    fetch(`${uri}/${id}`, {
      method: "PUT",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify(body),
    })
      .then((res) => {
        state.status = res.status;
        return res.json();
      })
      .then((resBody) => {
        state.response = resBody;
      })
      .catch((err) => {
        state.error = err;
        if (err.response) state.status = err.state.response;
      })
      .finally(() => {
        state.loading = false;
      });
  };

  return {...toRefs(state), request};
};

export default useHttpPut;
