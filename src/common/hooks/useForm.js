import {computed} from "vue";
import {every, keys} from "lodash";

const useForm = (form, model, rules) => {
  const validated = computed(() =>
    every(keys(rules), (key) =>
      every(rules[key], (rule) =>
        keys(model.value).includes(key) ? rule(model.value[key]) === true : true
      )
    )
  );

  return {
    validated,
  };
};

export default useForm;
