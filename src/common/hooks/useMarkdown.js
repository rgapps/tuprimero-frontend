import {keys, startCase} from "lodash";

export const useMarkdown = (title, data) => {
  const fields = keys(data);

  let result = "";

  result += `*${title.toUpperCase()}*\n`;

  for (let i = 0; i < fields.length; i++) {
    if (typeof data[fields[i]] === typeof [] && data[fields[i]]) {
      result += `*${startCase(fields[i])}:*\n`;
      for (let j = 0; j < data[fields[i]].length; j++) {
        result += useMarkdown(data[fields[i]][j].title, data[fields[i]][j]);
      }
    } else if (data[fields[i]]?.toString().trim())
      result += `\t*${startCase(fields[i])}:* ${data[fields[i]]}\n`;
  }

  return result;
};
