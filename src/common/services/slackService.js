import {reactive, toRefs} from "vue";
import {slackUri} from "@/environment";
import {useMarkdown} from "@/common/hooks/useMarkdown";

export const sendMessage = (title) => {
  const state = reactive({
    loading: false,
  });

  const send = (data) => {
    const result = useMarkdown(title, data);

    state.loading = true;
    fetch(slackUri, {
      method: "POST",
      body: JSON.stringify({text: result}),
    }).finally(() => {
      state.loading = false;
    });
  };

  return {...toRefs(state), send};
};
