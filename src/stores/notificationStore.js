import {ref} from "vue";
import {defineStore} from "pinia/dist/pinia";

export const notificationStore = defineStore("notifications", () => {
  const notifications = ref([]);

  const addNotification = (model, type) => {
    const id = crypto.randomUUID();
    notifications.value.unshift({...model, type, id});

    setTimeout(() => removeNotification(id), 7000);
  };

  const removeNotification = (id) => {
    const index = notifications.value.findIndex((n) => n.id === id);

    if (index >= 0) notifications.value.splice(index, 1);
  };

  return {
    notifications,
    addNotification,
    removeNotification,
  };
});
