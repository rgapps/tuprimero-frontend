import {defineStore} from "pinia";
import {computed} from "vue";
import {storeToRefs} from "pinia/dist/pinia";
import {useCategories} from "@/stores/categoriesStore";
import {useProducts} from "@/stores/productsStore";
import {omit} from "lodash";
import {ResourceType} from "@/model/resourceTypes";
import {useTags} from "@/stores/tagsStore";

export const useResources = defineStore("resources", () => {
  const stores = {
    [ResourceType.CATEGORY]: useCategories(),
    [ResourceType.PRODUCT]: useProducts(),
    [ResourceType.TAG]: useTags(),
  };

  const {
    categories,
    usedCategories,
    retrieving: retrievingCategories,
    saving: savingCategories,
    error: errorCategories,
  } = storeToRefs(useCategories());

  const {
    products,
    retrieving: retrievingProducts,
    saving: savingProducts,
    error: errorProducts,
  } = storeToRefs(stores[ResourceType.PRODUCT]);

  const {
    tags,
    usedTags,
    retrieving: retrievingTags,
    saving: savingTags,
    error: errorTags,
  } = storeToRefs(stores[ResourceType.TAG]);

  const resources = computed(() => [
    ...categories.value.map((category) => ({
      data: omit({...category}, "id"),
      id: category.id,
      type: ResourceType.CATEGORY,
    })),
    ...products.value.map((product) => ({
      data: omit({...product}, "id"),
      id: product.id,
      type: ResourceType.PRODUCT,
    })),
    ...tags.value.map((tag) => ({
      data: omit({...tag}, "id"),
      id: tag.id,
      type: ResourceType.TAG,
    })),
  ]);

  const isRemovable = (resource) => {
    switch (resource.type) {
      case ResourceType.TAG:
        return !usedTags.value.some((t) => t.id === resource.id);
      case ResourceType.CATEGORY:
        return !usedCategories.value.some((c) => c.id === resource.id);
      default:
        return true;
    }
  };

  const retrieve = (type) => stores[type].retrieve();
  const create = (resource) => stores[resource.type].create(resource.data);
  const update = (resource) => stores[resource.type].update(resource.id, resource.data);
  const remove = (resource) => stores[resource.type].remove(resource.id);

  const retrieving = computed(
    () => retrievingCategories.value || retrievingProducts.value || retrievingTags.value
  );
  const saving = computed(() => savingProducts.value || savingCategories.value || savingTags.value);
  const error = computed(() => errorProducts.value || errorCategories.value || errorTags.value);

  return {
    resources,
    create,
    retrieve,
    update,
    remove,
    isRemovable,
    saving,
    retrieving,
    error,
  };
});
