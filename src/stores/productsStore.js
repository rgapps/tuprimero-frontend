import {defineStore} from "pinia";
import {productsService} from "@/services/crudServices";
import {computed} from "vue";

export const useProducts = defineStore("products", () => {
  productsService.retrieve();

  const products = computed(() =>
    productsService.retrieving.value || productsService.errorRetrieving.value
      ? []
      : productsService.retrieved.value
  );

  return {
    products,
    ...productsService,
  };
});
