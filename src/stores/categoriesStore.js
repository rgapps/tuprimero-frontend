import {defineStore} from "pinia";
import {categoriesService} from "@/services/crudServices";
import {computed} from "vue";
import {storeToRefs} from "pinia/dist/pinia";
import {useProducts} from "@/stores/productsStore";

export const useCategories = defineStore("categories", () => {
  const {products, retrieving: retrievingProducts} = storeToRefs(useProducts());

  categoriesService.retrieve();

  const categories = computed(() =>
    categoriesService.retrieving.value || categoriesService.errorRetrieving.value
      ? []
      : categoriesService.retrieved.value
  );

  const usedCategories = computed(() => {
    if (!categoriesService.retrieving.value && !retrievingProducts.value)
      return categories.value.filter(({title}) =>
        products.value.some(({category}) => category === title)
      );
    return [];
  });

  return {
    categories,
    usedCategories,
    ...categoriesService,
  };
});
