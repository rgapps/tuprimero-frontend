import {computed} from "vue";
import {configurationService} from "@/services/crudServices";
import {defineStore} from "pinia/dist/pinia";
import {DefaultConfigurations} from "@/model/configurationTypes";
import {keyBy} from "lodash";

export const useConfigurations = defineStore("confs", () => {
  configurationService.retrieve();

  const notSavedConfigurations = computed(() =>
    configurationService.retrieving.value || configurationService.errorRetrieving.value
      ? DefaultConfigurations
      : DefaultConfigurations.filter(
          (defConfig) =>
            !configurationService.retrieved.value.some((c) => c.type === defConfig.type)
        )
  );

  const configurations = computed(() =>
    configurationService.retrieving.value || configurationService.errorRetrieving.value
      ? DefaultConfigurations
      : [...configurationService.retrieved.value, ...notSavedConfigurations.value]
  );

  const mappedConfigurations = computed(() => keyBy(configurations.value, "type"));

  return {
    configurations,
    mappedConfigurations,
    ...configurationService,
  };
});
