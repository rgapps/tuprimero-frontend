import {defineStore} from "pinia";
import {ordersService} from "@/services/crudServices";
import {computed, watch} from "vue";
import {OrderStatus} from "@/model/OrderStatus";
import {omit} from "lodash";

export const useOrdersStore = defineStore("orders", () => {
  ordersService.retrieve();

  const orders = computed(() =>
    ordersService.retrieving.value || ordersService.errorRetrieving.value
      ? []
      : ordersService.retrieved.value
  );

  const activeOrders = computed(() => orders.value.filter((o) => o.status === OrderStatus.ACTIVE));

  const completedOrders = computed(() =>
    orders.value.filter((o) => o.status === OrderStatus.COMPLETED)
  );

  const cancelledOrders = computed(() =>
    orders.value.filter((o) => o.status === OrderStatus.CANCELLED)
  );

  const loading = computed(
    () =>
      ordersService.updating.value ||
      ordersService.creating.value ||
      ordersService.deleting.value ||
      ordersService.retrieving.value
  );

  const changeOrderStatus = (order, status) => {
    const id = order.id;
    const body = {...omit(order, "id"), status};

    ordersService.update(id, body);
  };

  const activateOrder = (order) => changeOrderStatus(order, OrderStatus.ACTIVE);
  const cancelOrder = (order) => changeOrderStatus(order, OrderStatus.CANCELLED);
  const completeOrder = (order) => changeOrderStatus(order, OrderStatus.COMPLETED);

  watch(ordersService.updating, (value) => {
    if (value) return;
    ordersService.retrieve();
  });

  watch(ordersService.creating, (value) => {
    if (value) return;
    ordersService.retrieve();
  });

  watch(ordersService.deleting, (value) => {
    if (value) return;
    ordersService.retrieve();
  });

  return {
    orders,
    activeOrders,
    completedOrders,
    cancelledOrders,
    loading,
    ...ordersService,
    activateOrder,
    cancelOrder,
    completeOrder,
  };
});
