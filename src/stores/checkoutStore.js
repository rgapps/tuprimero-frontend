import {defineStore} from "pinia";
import {computed, ref} from "vue";

export const useCheckoutStore = defineStore("checkout", () => {
  const cart = ref([]);

  const currency = ref("AED");

  const orderDetails = ref(null);

  const total = computed(() => {
    let count = 0;
    cart.value.forEach((i) => (count += parseInt(i.cost) * i.count));
    return count;
  });

  const cartCount = computed(() => {
    let count = 0;
    cart.value.forEach((i) => (count += i.count));
    return count;
  });

  const cartIsEmpty = computed(() => cartCount.value === 0);

  const itemIndex = (item) => {
    return cart.value.findIndex((i) => i.title === item.title && i.cost === item.cost);
  };

  const addItem = (item) => {
    const index = itemIndex(item);
    if (index >= 0) cart.value[index].count += 1;
    else cart.value.push({...item, count: 1});
  };

  const removeItem = (item) => {
    const index = itemIndex(item);
    if (index >= 0 && cart.value[index].count > 1) cart.value[index].count -= 1;
    else if (index >= 0) cart.value.splice(index, 1);
  };

  const clearCart = () => {
    cart.value = [];
  };

  const checkout = (order) => {
    orderDetails.value = order;
    clearCart();
  };

  return {
    cart,
    cartCount,
    cartIsEmpty,
    currency,
    total,
    orderDetails,
    addItem,
    checkout,
    clearCart,
    itemIndex,
    removeItem,
  };
});
