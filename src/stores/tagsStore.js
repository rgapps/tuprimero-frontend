import {defineStore, storeToRefs} from "pinia";
import {computed} from "vue";
import {categoriesService, tagsService} from "@/services/crudServices";
import {useProducts} from "@/stores/productsStore";

export const useTags = defineStore("tags", () => {
  const {products, retrieving: retrievingProducts} = storeToRefs(useProducts());

  tagsService.retrieve();

  const tags = computed(() =>
    tagsService.retrieving.value || tagsService.errorRetrieving.value
      ? []
      : tagsService.retrieved.value.map((tag) => {
          tag["hasIcon"] = tag["hasIcon"] === "true";
          tag["animated"] = tag["animated"] === "true";
          return tag;
        })
  );

  const usedTags = computed(() => {
    if (!categoriesService.retrieving.value && !retrievingProducts.value)
      return tags.value.filter(({id}) => products.value.some(({tagId}) => tagId === id));
    return [];
  });

  return {
    tags,
    usedTags,
    ...tagsService,
  };
});
