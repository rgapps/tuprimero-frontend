import ResourcesPage from "@/pages/admin/ResourcesPage.vue";
import ConfigurationPage from "@/pages/admin/ConfigurationPage.vue";
import OrdersPage from "@/pages/admin/OrdersPage.vue";

const routes = [
  {path: "", name: "resources", component: ResourcesPage},
  {path: "configuration", name: "configuration", component: ConfigurationPage},
  {path: "orders", name: "orders", component: OrdersPage},
];

export default routes;
