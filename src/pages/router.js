import {createRouter, createWebHistory} from "vue-router";
import HomePage from "./HomePage.vue";
import ProductsPage from "./ProductsPage.vue";
import AboutPage from "./AboutPage.vue";
import ContactPage from "./ContactPage.vue";
import adminRoutes from "./admin/router";
import checkoutRoutes from "./checkout/router";

const routes = [
  {path: "/", name: "home", component: HomePage},
  {path: "/products", name: "products", component: ProductsPage},
  {path: "/about", name: "about", component: AboutPage},
  {path: "/contact", name: "contact", component: ContactPage},
  {path: "/checkout", children: checkoutRoutes},
  {path: "/admin", children: adminRoutes},
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
});
export default router;
