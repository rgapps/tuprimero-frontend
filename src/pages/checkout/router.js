import CheckoutPage from "@/pages/checkout/CheckoutPage.vue";
import DeliveryPage from "@/pages/checkout/DeliveryPage.vue";
import OrderReceived from "@/pages/checkout/OrderReceived.vue";

const routes = [
  {path: "", name: "checkout", component: CheckoutPage},
  {path: "delivery", name: "delivery", component: DeliveryPage},
  {path: "success", name: "success-checkout", component: OrderReceived},
];

export default routes;
