import {backendUri} from "@/environment";
import {crudService} from "@/common/services/crudService";

export const categoriesService = crudService(`${backendUri}/data/categories`);
export const productsService = crudService(`${backendUri}/data/products`);
export const tagsService = crudService(`${backendUri}/data/tags`);

export const ordersService = crudService(`${backendUri}/data/orders`);

export const configurationService = crudService(`${backendUri}/data/configurations`);
