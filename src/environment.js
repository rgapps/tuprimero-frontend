export const slackUri = import.meta.env.VITE_SLACK_URI;
export const backendUri = import.meta.env.VITE_BACKEND_URI || "http://localhost:8080/api";
