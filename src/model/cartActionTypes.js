export const CartActionTypes = Object.freeze({
  REMOVE: "remove",
  ADD: "add",
});
