export const ConfigurationTypes = Object.freeze({
  ABOUT_TEXT: "about text",
  LOGO: "logo",
  COMPANY_NAME: "company name",
  ADDRESS: "company address",
  CONTACT_NO: "contact number",
});

export const DefaultConfigurations = [
  {
    type: ConfigurationTypes.ABOUT_TEXT,
    value: "Describe what's the page about",
  },
  {
    type: ConfigurationTypes.LOGO,
    value: "/logo-md.svg",
  },
  {
    type: ConfigurationTypes.COMPANY_NAME,
    value: "MyCompany",
  },
  {
    type: ConfigurationTypes.ADDRESS,
    value: "My address",
  },
  {
    type: ConfigurationTypes.CONTACT_NO,
    value: "",
  },
];
