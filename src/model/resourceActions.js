export const ResourceAction = Object.freeze({
  CREATE: "create",
  EDIT: "edit",
  DELETE: "delete",
});
