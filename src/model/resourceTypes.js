export const ResourceType = Object.freeze({
  PRODUCT: "Product",
  CATEGORY: "Category",
  TAG: "Tag",
});

export const EmptyResources = {
  [ResourceType.PRODUCT]: {
    title: "",
    category: "",
    description: "",
    cost: "",
    image: "",
    tagId: "",
    currency: "AED",
  },
  [ResourceType.CATEGORY]: {
    title: "",
    description: "",
    image: "",
  },
  [ResourceType.TAG]: {
    label: "",
    icon: "",
    iconColor: "",
    iconPosition: "end",
    color: "#222",
    textColor: "#b2b2b2",
    animated: false,
    hasIcon: false,
  },
};
