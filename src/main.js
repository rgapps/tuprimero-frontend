import {createApp} from "vue";
import {createPinia} from "pinia";
import {createVuetify} from "vuetify";
import App from "./App.vue";
import router from "./pages/router";
import "@mdi/font/css/materialdesignicons.css";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "vuetify/styles";
import "./main.scss";
import {aliases, mdi} from "vuetify/iconsets/mdi";
import {md} from "vuetify/iconsets/md";
import {fa} from "vuetify/iconsets/fa";
import clickOutside from "@/common/directives/clickOutside";

if (import.meta.env.DEV) {
  const originalError = console.error;

  const alertOnDevUnexpectedLogs = (args) => {
    const now = new Date().getSeconds();
    if (now - time > 20) {
      originalError(args.join());
      alert("Please Fix: " + args[0]);
      time = now;
    }
  };

  let time = new Date().getSeconds() - 21;
  console.warn = function (...args) {
    alertOnDevUnexpectedLogs(args);
  };
  console.error = function (...args) {
    alertOnDevUnexpectedLogs(args);
  };
}

const vuetify = createVuetify({
  icons: {
    defaultSet: "mdi",
    aliases,
    sets: {md, mdi, fa},
  },
});

const app = createApp(App);

app.directive("click-outside", clickOutside);

app.use(createPinia());
app.use(vuetify);
app.use(router);

app.mount("#app");
